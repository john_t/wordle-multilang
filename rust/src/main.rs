use std::io;

#[derive(Copy, Clone)]
enum MatchLevel {
    Wrong, 
    Partial,
    Full
}

fn colour_wrong(c: char) -> String {
    format! ("\x1b[2m{}\x1b[0m", c)
}
fn colour_right(c: char) -> String {
    format! ("\x1b[92m{}\x1b[0m", c)
}
fn colour_partial(c: char) -> String {
    format! ("\x1b[93m{}\x1b[0m", c)
}

fn main() -> io::Result<()>{

    let stdin = io::stdin();

    let answers_list = include_str! ("../../words.txt");
    let answers_list = answers_list.split('\n');
    let answers_list: Vec<&str> = answers_list.collect();

    let possibilities_list = include_str! ("../../possibilites.txt");
    let possibilities_list = possibilities_list.split('\n');
    let possibilities_list: Vec<&str> = possibilities_list.collect();
    
    let answer = answers_list[fastrand::usize(..answers_list.len())];
    //let answer = "weedy"

    println!("Welcome to Wordle-CLI! Enter your guess to begin!");

    let mut guess = String::new();

    while guess != answer{
        guess = String::new();
        stdin.read_line(&mut guess)?;
        let guess = guess.trim();

        if !answers_list.contains(&guess) && !possibilities_list.contains(&guess){
            println!("Not a valid word");
            continue;
        }

        let mut output = [MatchLevel::Wrong; 5];

        let mut greens = Vec::new();

        for (i, (answer_letter, guess_letter)) in answer.chars().zip(guess.chars()).enumerate(){
            if guess_letter == answer_letter{
                output[i] = MatchLevel::Full;
                greens.push(guess_letter);
            }
        }

        for (i, guess_letter) in guess.chars().enumerate(){
            if answer.contains(guess_letter) && ! greens.contains(&guess_letter){
                output[i] = MatchLevel::Partial;
            }
        }

        let mut printable = String::new();

        for (l, letter) in output.iter().zip(guess.chars()){
            match l{
                MatchLevel::Full => printable.push_str(&colour_right(letter)),
                MatchLevel::Partial => printable.push_str(&colour_partial(letter)),
                MatchLevel::Wrong => printable.push_str(&colour_wrong(letter)),
            }
        }
        print!("\x1b[A\x1b[K");
        println!("{}", printable)
    }
    Ok(())
}
