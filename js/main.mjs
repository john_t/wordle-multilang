#!/usr/bin/env node

import * as fs from 'node:fs/promises';
import { stdin, stdout } from 'node:process';
import * as readline from 'node:readline/promises';

const rl = readline.createInterface({
    input: stdin,
    output: stdout,
    prompt: "",
});

/************************
** Stylistic functions **
************************/

function colourWrong(s) {
    return "\x1b[2m" + s + "\x1b[0m";
}

function colourRight(s) {
    return "\x1b[92m" + s + "\x1b[0m";
}

function colourPartial(s) {
    return "\x1b[93m" + s + "\x1b[0m";
}

async function main() {
    /***************
    ** Read words **
    ***************/

    let answersFilef = await fs.readFile("../words.txt");
    let answersList = answersFilef.toString("utf8").split('\n');

    let possiblesFilef = await fs.readFile("../possibilites.txt");
    let possiblesList = possiblesFilef.toString("utf8").split('\n');

    let answer = answersList[Math.floor(Math.random() * answersList.length)]

    console.log("Welcome to Wordle-CLI! Enter your guess to begin!")

    let guess = ""
    while (guess != answer) {
        /***********************
        ** Input and validate **
        ***********************/

        guess = await rl.question("");

        if (!(answersList.includes(guess)) && !(possiblesList.includes(guess))) {
            console.log("Not a valid word");
            continue;
        }

        /*****************
        ** Style Output **
        *****************/

        // Assume everything is wrong by default
        let output = [];
        for (let i = 0; i < guess.length; i++) {
            output.push(0);
        }

        // Work out the greens
        let greens = [];
        for (let i = 0; i < guess.length; i++) {
            if (guess[i] == answer[i]) {
                output[i] = 2;
                greens.push(guess[i]);
            }
        }

        // Work out the yellows
        for (let i = 0; i < guess.length; i++) {
            if (answer.includes(guess[i]) &&!greens.includes(guess[i])) {
                output[i] = 1;
            }
        }

        // Format for printing
        let printable = ""
        for (let i = 0; i < output.length; i++) {
            switch (output[i]) {
                case 2:
                    printable += colourRight(guess[i]);
                    break;
                case 1:
                    printable += colourPartial(guess[i]);
                    break;
                default:
                    printable += colourWrong(guess[i]);
                    break;
            }
        }

        console.log("\x1b[A" + printable);
    }
}

await main();
