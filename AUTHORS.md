# Authors

Here are the people who contributed to this project.

John Toohey <john_t@mailo.com> @john_t:

 * c
 * js
 * python
 * ruby
 * sh

Seth Rogers <seth_henry@icloud.com> @Thesnake66six:

 * c#
 * rust
 * python
 * go
