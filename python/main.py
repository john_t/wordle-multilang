#!/usr/bin/env python

import random as rnd
import colorama as colour

#########################
## Stylistic functions ##
#########################

def colour_wrong(s: str) -> str:
    return colour.Style.DIM + s + colour.Style.RESET_ALL

def colour_right(s: str) -> str:
    return colour.Fore.GREEN + colour.Style.BRIGHT + s + colour.Style.RESET_ALL

def colour_partial(s: str) -> str:
    return colour.Fore.YELLOW + colour.Style.BRIGHT + s + colour.Style.RESET_ALL

def main():
    colour.init()

    ################
    ## Read words ##
    ################

    filef = open("../words.txt", "r")
    answers_list = []
    for word in filef:
        answers_list.append(word.strip())

    filef = open ("../possibilites.txt", "r")
    possibles_list = []
    for word in filef:
        possibles_list.append(word.strip())

    answer = answers_list[rnd.randint(0, len(answers_list))]

    print("Welcome to Wordle-CLI! Enter your guess to begin!")

    guess = ""
    while guess != answer:

        ########################
        ## Input and validate ##
        ########################

        guess = input()

        if guess not in answers_list and guess not in possibles_list:
            print("Not a valid word")
            continue

        ##################
        ## Style Output ##
        ##################

        # Assume everything is wrong by default
        output = []
        for l in guess:
            output.append((l, 0))

        greens = []

        # Work out the greens
        i = 0
        for (answer_letter, guess_letter) in zip(answer, guess):
            if guess_letter == answer_letter:
                output[i] = (guess_letter, 2)
                greens.append(guess_letter)
            i += 1

        # Work out the yellows
        i = 0
        for guess_letter in guess:
            if guess_letter in answer and output[i][1] != 2 and guess_letter not in greens:
                output[i] = (guess_letter, 1)
            i += 1

        # Format for printing
        printable = ""
        for l in output:
            if l[1] == 2:
                printable += colour_right(l[0])
            elif l[1] == 1:
                printable += colour_partial(l[0])
            else:
                printable += colour_wrong(l[0])

        print(colour.Cursor.UP(), end = "")
        print(colour.ansi.clear_line(), end = "")
        print(printable)

if __name__ == '__main__':
    main()
