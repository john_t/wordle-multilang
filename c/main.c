#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#define WORDS_LEN 2315
#define POSSIBILITIES_LEN 10657
#define WORD_LEN 5

enum Level_t {
    LEVEL_NO_MATCH,
    LEVEL_PARTIAL,
    LEVEL_FULL,
}; typedef enum Level_t Level;

struct CharMatch_t {
    char chr;
    Level level;
}; typedef struct CharMatch_t CharMatch;

/*************************
 ** Stylistic functions **
 *************************/

char * color_wrong(char c) {
    char * output = calloc(sizeof(char), 10);
    sprintf(output, "\x1b[2m%c\x1b[0m", c);
    return output;
}

char * color_right(char c) {
    char * output = calloc(sizeof(char), 11);
    sprintf(output, "\x1b[92m%c\x1b[0m", c);
    return output;
}

char * color_partial(char c) {
    char * output = calloc(sizeof(char), 11);
    sprintf(output, "\x1b[93m%c\x1b[0m", c);
    return output;
}

bool contains(const void * arr, const void * check, size_t elem_size, int length) {
    for (int i = 0; i < length; i++) {
        if (memcmp(arr + elem_size * i, check, elem_size) == 0) {
            return true;
        }
    }

    return false;
}

int main(int argc, char **argv) {
    FILE * filef;
    char answers_list[WORDS_LEN][WORD_LEN + 1];
    FILE * possibles_filef;
    char possibles_list[POSSIBILITIES_LEN][WORD_LEN + 1];
    int answer_n;
    char * answer;
    char guess[WORD_LEN];

    memset(guess, '\0', WORD_LEN);

    /****************
     ** Read Words **
     ****************/

    // Read in the normal words
    filef = fopen("../words.txt", "r");
    for (int i = 0; i < WORDS_LEN; i++) {
        fgets(answers_list[i], WORD_LEN + 1, filef);
        fgetc(filef);
        answers_list[i][WORD_LEN] = 0;
    }
    fclose(filef);

    // Read in the allowed guesses
    possibles_filef = fopen("../possibilites.txt", "r");
    for (int i = 0; i < POSSIBILITIES_LEN; i++) {
        fgets(possibles_list[i], WORD_LEN + 1, possibles_filef);
        fgetc(possibles_filef);
        possibles_list[i][WORD_LEN] = 0;
    }
    fclose(possibles_filef);

    srand(time(NULL));
    answer_n = rand() % WORDS_LEN;
    answer = answers_list[answer_n];

    printf("Welcome to Wordle-CLI! Enter your guess to begin!\n");

    while (strcmp(guess, answer)) {
        /************************
         ** Input and validate **
         ************************/

        char guess[6];
        bool in_answers_list;
        bool in_possibles_list;

        scanf("%5s", guess);

        in_answers_list = contains(&answers_list, &guess, sizeof(char) * (WORD_LEN + 1), WORDS_LEN);
        in_possibles_list = contains(&possibles_list, &guess, sizeof(char) * (WORD_LEN + 1), WORDS_LEN);

        if (!in_answers_list && !in_possibles_list) {
            printf("Not a valid word\n");
            continue;
        }

        /******************
         ** Style Output **
         ******************/

        // Assume everything is wrong by default
        CharMatch output[WORD_LEN];
        for (int i = 0; i < WORD_LEN; i++) {
            output[i].chr = guess[i];
            output[i].level = LEVEL_NO_MATCH;
        }

        char greens[WORD_LEN];
        memset(greens, 0, 5);
        int green_idx = 0;

        // Work out the greens
        for (int i = 0; i < WORD_LEN; i++) {
            if (guess[i] == answer[i]) {
                output[i].level = LEVEL_FULL;
                greens[green_idx] = guess[i];
                green_idx++;
            }
        }

        // Work out the yellows
        for (int i = 0; i < WORD_LEN; i++) {
            bool in_answer;
            bool in_greens;

            in_answer = contains(answer, &guess[i], sizeof(char), WORD_LEN);
            in_greens = contains(&greens, &guess[i], sizeof(char), green_idx);

            if (in_answer && !in_greens) {
                output[i].level = LEVEL_PARTIAL;
            }
        }

        char printable[56];
        int printable_idx = 0;
        memset(printable, '\0', 56);
        for (int i = 0; i < WORD_LEN; i++) {
            if (output[i].level == LEVEL_FULL) {
                char * formatted = color_right(output[i].chr);
                strcpy(printable + printable_idx, formatted);
                printable_idx += strlen(formatted);
                free(formatted);
            }
            else if (output[i].level == LEVEL_PARTIAL) {
                char * formatted = color_partial(output[i].chr);
                strcpy(printable + printable_idx, formatted);
                printable_idx += strlen(formatted);
                free(formatted);
            }
            else {
                char * formatted = color_wrong(output[i].chr);
                strcpy(printable + printable_idx, formatted);
                printable_idx += strlen(formatted);
                free(formatted);
            }
        }

        printf("\x1b[A");
        printf("%s\n", printable);
    }
    
    return 0;
}
