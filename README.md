# Wordle*

Wordle, in all the programming languages.

The aim of this project is to provide a simple project that scales
well across programming languages to see a comparisson of programming
languages, a bit like [TodoMVC](https://todomvc.com/) but for
programming languages.

The guidance for an entry are as follows:

 * The user experience MUST be exactly the same (except for
   processing delays)
 * You MAY assume the files (words.txt, possibilities.txt) are present
   at compile time
 * You MAY assume that the files are always in the same location
   at runtime.
 * You MAY assume that the files will stay the same

Any merge requests welcome!

Examples licensed under the AGPL v3.0 or later, documentation
licensed under the GNU Free Documentation License.
