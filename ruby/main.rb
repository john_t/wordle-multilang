#!/usr/bin/env ruby

#########################
## Stylistic functions ##
#########################

def color_wrong(s)
    return "\x1b[2m" + s + "\x1b[0m"
end

def color_right(s)
    return "\x1b[92m" + s + "\x1b[0m"
end

def color_partial(s)
    return "\x1b[93m" + s + "\x1b[0m"
end

def main()

    ################
    ## Read words ##
    ################

    filef = File.new "../words.txt", "r" 
    answers_list = []
    for word in filef
        answers_list << word.strip 
    end

    filef = File.new "../possibilites.txt", "r" 
    possibles_list = []
    for word in filef
        possibles_list << word.strip 
    end

    answer = answers_list.sample

    puts "Welcome to Wordle-CLI! Enter your guess to begin!"

    guess = ""
    while guess != answer do

         ########################
         ## Input and validate ##
         ########################

         guess = STDIN.gets.chomp

         if not answers_list.include? guess and not possibles_list.include? guess
             puts "Not a valid word"
             redo
         end

         ##################
         ## Style Output ##
         ##################

         # Assume everything is wrong by default
         output = []
         for i in 0..(guess.length - 1) do
             output.append([guess[i], 0])
         end

         greens = []

         # Work out the greens
         for i in 0..(guess.length - 1) do
             if guess[i] == answer[i]
                 output[i] = [guess[i], 2]
                 greens << guess[i]
             end
         end

         # Work out the yellows
         for i in 0..(guess.length - 1) do
             if answer.include? guess[i] and not greens.include? guess[i]
                 output[i] = [guess[i], 1]
             end
         end

         # Format for printing
         printable = ""
         for l in output do
             case
             when l[1] == 2 then printable += color_right(l[0])
             when l[1] == 1 then printable += color_partial(l[0])
             when l[1] == 0 then printable += color_wrong(l[0])
             end
         end
         print "\x1b[A"
         print "\x1b[K"
         puts printable
    end
end

main()
