#!/bin/sh

#########################
## Stylistic functions ##
#########################

colour_wrong() {
    echo "\033[2m${1}\033[0m"
}

colour_right() {
    echo "\033[92m${1}\033[0m"
}

colour_partial() {
    echo "\033[93m${1}\033[0m"
}

################
## Read words ##
################

WORDS="../words.txt"
POSSIBLITIES="../possibilites.txt"

ANSWER=$(shuf -n1 "$WORDS")

echo "Welcome to Wordle-CLI! Enter your guess to begin!"

GUESS=""
while [ "$GUESS" != "$ANSWER" ]; do
    OUTPUT=""

    ########################
    ## Input and validate ##
    ########################

    read -r GUESS

    if not grep -Fqx "$GUESS" "$WORDS" "$POSSIBLITIES"; then
        echo "Not a valid word"
        continue
    fi

    ##################
    ## Style Output ##
    ##################

    # Assume everything is wrong by default
    OUTPUT=""
    for _ in $(seq 1 5); do
        OUTPUT="0
${OUTPUT}"
    done

    GREENS=""

    # Work out the greens
    for i in $(seq 1 $(expr $(echo "$GUESS" | wc -m) - 1)); do
        GUESS_LETTER=$(echo "$GUESS" | cut -c "$i")
        ANSWER_LETTER=$(echo "$ANSWER" | cut -c "$i")
        if [ "$GUESS_LETTER" = "$ANSWER_LETTER" ]; then
            OUTPUT=$(echo "$OUTPUT" | sed "${i}s/./2/")
            GREENS="${GREENS}${GUESS_LETTER}"
        fi
    done

    # Work out the yellows
    for i in $(seq 1 $(expr $(echo "$GUESS" | wc -m) - 1)); do
        GUESS_LETTER=$(echo "$GUESS" | cut -c "$i")
        if echo "$ANSWER" | grep -Fq "$GUESS_LETTER" && echo "$GREENS" | grep -Fq "$GUESS_LETTER"; then
            OUTPUT=$(echo "$OUTPUT" | sed "${i}s/./1/")
        fi
    done

    # Format for printing
    PRINTABLE=""
    for i in $(seq 1 5); do
        LETTER=$(echo "$GUESS" | cut -c "$i")
        VAL=$(echo "$OUTPUT" | sed "${i}q;d")
        case $VAL in
            '2') PRINTABLE="${PRINTABLE}$(colour_right "${LETTER}")";;
            '1') PRINTABLE="${PRINTABLE}$(colour_partial "${LETTER}")";;
            *) PRINTABLE="${PRINTABLE}$(colour_wrong "${LETTER}")";;
        esac
    done

    printf "\033[A"
    printf "\033[K"
    printf "%b\n" "$PRINTABLE"
done
