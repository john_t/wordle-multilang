package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
	"unsafe"
)

func IntToByteArray(num int) []byte {
  size := int(unsafe.Sizeof(num))
  arr := make([]byte, size)
  for i := 0 ; i < size ; i++ {
      byt := *(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&num)) + uintptr(i)))
      arr[i] = byt
  }
  return arr
}

func contains_string(s string, e byte) bool {
  for i := 0; i < len(s); i++{
    if s[i] == e {
      return true
    }
  }
  return false
}

func contains_string_array(s []string, e string) bool {
  for _, a := range s {
    if a == e {
      return true
    }
  }
  return false
}

/************************
** Stylistic functions **
************************/

func colour_wrong(s string) string {
  return string("\033[2m" + s + "\033[0m")
}
func colour_right(s string) string {
  return string("\033[92m" + s + "\033[0m")
}
func colour_partial(s string) string {
  return string("\033[93m" + s + "\033[0m")
}

func main() { 

  /***************
  ** Read words **
  ***************/

  rand.Seed(time.Now().UTC().UnixNano())
  
  var answers_list []string
  
  filea, erra := os.Open("../words.txt")
  if erra != nil {
    fmt.Println(erra)
  }
  defer filea.Close()
  
  scannera := bufio.NewScanner(filea)
  for scannera.Scan() {
    answers_list = append(answers_list, scannera.Text())
  }
  
  if err := scannera.Err(); err != nil {
    fmt.Println(err)
  }
  
  var possibles_list []string
  
  fileb, errb := os.Open("../words.txt")
  if errb != nil {
    fmt.Println(errb)
  }
  defer fileb.Close()
  
  scannerb := bufio.NewScanner(fileb)
  for scannerb.Scan() {
    possibles_list = append(possibles_list, scannerb.Text())
  }
  
  if err := scannerb.Err(); err != nil {
    fmt.Println(err)
  }
  
  answer := answers_list[rand.Intn(len(answers_list) - 1)]
  println(answer)
  
  fmt.Println("Welcome to Wordle-CLI! Enter your guess to begin!")
  
  var guess string
  
  for i:=0; guess != answer; i++{
    /***********************
    ** Input and validate **
    ***********************/

    fmt.Scanf("%s", &guess)
    
    if ! contains_string_array(answers_list, guess) && ! contains_string_array(possibles_list, guess){
      fmt.Println("Not a valid word")
    }
  
    /*****************
    ** Style Output **
    *****************/

    // Assume everything is wrong by default
    var output []int
    for i := 0; i < len(guess); i++{
      output = append(output, 0)
    }
  
    // Work out the greens
    var greens = ""
    for i := 0; i < len(guess); i++{
      if guess[i] == answer[i]{
        output[i] = 2
        greens += string(guess[i]);
      }
    }

    // Work out the yellows
    for i := 0; i < len(guess); i++{
      if contains_string(answer, guess[i]) && !contains_string(greens, guess[i]) {
        output[i] = 1
      }
    }

    // Format for printing
    var printable = ""
    for i := 0; i < len(output); i++{
      switch output[i]{
      case 2:
        printable += colour_right(string(guess[i]))
      case 1:
        printable += colour_partial(string(guess[i]))
      case 0:
        printable += colour_wrong(string(guess[i]))
      }
    }
    print("\033[A" + "\033[K")
    println(printable)
  }
}