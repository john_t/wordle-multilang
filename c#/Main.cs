﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] answersList = File.ReadAllLines("../words.txt"); 
            string[] possiblesList = File.ReadAllLines("../possibilites.txt"); 

            Random rnd = new Random();
            string answer = answersList[rnd.Next(answersList.Length)];

            Console.WriteLine("Welcome to Wordle-CLI! Enter your guess to begin!");

            string guess = "";
            while (guess != answer)
            {
                guess = Console.ReadLine();

                if (!(answersList.Contains(guess) || possiblesList.Contains(guess)))
                {
                    Console.WriteLine("Not a valid word");
                    continue;
                }

                Dictionary<string, int> output = new Dictionary<string, int>(); 
                int i = 0;
                foreach (char l in guess)
                {
                    output.Add(l.ToString() + i.ToString(), 0);
                    i++;
                }

                List<string> greens = new List<string>();

                for (i = 0; i <5; i++)
                {
                    char guessLetter = guess[i];
                    char answerLetetr = answer[i];
                    
                    if (guessLetter == answerLetetr)
                    {
                        output[guessLetter.ToString() + i.ToString()] = (2);
                        greens.Add(guessLetter.ToString());
                    }
                }

                for (i = 0; i < 5; i++)
                {
                    char guessLetter = guess[i];
                    char answerLetetr = answer[i];

                    if (answer.Contains(guessLetter) && output[guessLetter + i.ToString()] != 2 && !greens.Contains(guessLetter.ToString()))
                    {
                        output[guessLetter + i.ToString()] = (1);
                    }
                }

                string printable = "";

                i = 0;
                foreach (KeyValuePair<string, int> l in output)
                {
                    switch (l.Value)
                    {
                        case 2:
                            printable += colour_right(l.Key[0]);
                            break;
                        case 1:
                            printable += colour_partial(l.Key[0]);
                            break;
                        case 0:
                            printable += colour_wrong(l.Key[0]);
                            break;
                    }
                }

                Console.Write("\x1b[A" + "\x1b[K");
                Console.WriteLine(printable);
            }
        }

        static string colour_wrong(char c)
        {
            return "\x1b[2m" + c + "\x1b[0m";
        }
        static string colour_right(char c)
        {
            return "\x1b[92m" + c + "\x1b[0m";
        }
        static string colour_partial(char c)
        {
            return "\x1b[93m" + c + "\x1b[0m";
        }
        
    }
}
